#!/bin/sh
set -xue
podman pull fedora:rawhide
exec podman run --rm -it fedora:rawhide sh -c '
	dnf upgrade -y --refresh &&
	dnf install automake bison flex gcc gettext-devel git openssl-devel -y && 
	git clone https://gitlab.com/fetchmail/fetchmail.git && 
	cd fetchmail && 
	autoreconf -svif && 
	./configure -C && 
	make -sj20 check && 
	./fetchmail -V'
