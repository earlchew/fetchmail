// obtain time zone offset. Not currently linked to builds.
// build libfm.a, then: cc -I. test-env.c libfm.a -o test-env
// may need -lintl and -L option.

#include "env.c"

const char *program_name;
char *user;
char *name;
char *home;
char *rcfile;
char *fmhome;
int at_home;
char *fm_realpath(const char *unused) { (void)unused; return ""; };

int main(int argc, char **argv)
{
	(void) argc;
	(void) argv;

	tzset();
	printf("tzname: %s, %s\n", tzname[0], tzname[1]);
	puts(rfc822timestamp());
	exit(0);
}
